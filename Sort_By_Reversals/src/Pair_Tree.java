//import statements
import java.util.ArrayList;

/**
 * 
 * @author BRAULT Benjamin 
 * @version 0.1
 * @since 0.0
 */

public class Pair_Tree {
	
	/* -----------------------------------------------------------------
	 * Attribut des Pair Tree's
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * Valeur x pour l'affichage
	 */
	private int x;
	/**
	 * Valeur y pour l'affichage
	 */
	private int y;
	
	/**
	 * Valeur du noeud.
	*/
	private int val;
	/**
	 * Position dans le bloc du début.
	 */
	private int positionBloc;
	//private boolean estDansV;
	/**
	 * Nombre d'arc(s) orienté(s) dans les fils du noeud
	 */
	private int nbArcOrienté;
	//private int nbArcNonOrienté;
	/**
	 * Flag définissant le sens de lecture du noeud et de ses fils.
	 */
	private boolean estInversé;
	/**
	 * Flag définissant l'orientation de l'arc 
	 */
	private boolean estOrienté;
	/**
	 * Fils gauche du noeud.
	 */
	private Pair_Tree fg;
	/**
	 * Fils droit du noeud.
	 */
	private Pair_Tree fd;
	/**
	 * Successeur du noeud (peut se trouver dans un autre arbre)
	 */
	private Pair_Tree successeur;
	/**
	 * Flag définissant si le noeud est vide. 
	 */
	private boolean vide;
	/**
	 * Balance du noeud. 
	 */
	private int bal;
	/**
	 * Hauteur d'un noeud
	 */
	private int ht;
	/**
	 * Homologue dans le search tree du même bloc
	 */
	private Search_Tree pos;
	
	/* -----------------------------------------------------------------
	 * Constructeur(s) des Pair Tree's
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * Initialisation par défaut.
	 */
	public Pair_Tree(){
		vide = true;
		fg = null;
		fd = null;
		bal = 0;
		ht = 0;
		estOrienté = false;
	}
	
	/**
	 * Initialisation par valeur, fils gauche et fils droit.
	 * @param p_val {@link #val}
	 * @param p_fg {@link #fg}
	 * @param p_fd {@link #fd}
	 */
	public Pair_Tree(int p_val, Pair_Tree p_fg, Pair_Tree p_fd){
		val = p_val;
		fg = p_fg;
		fd = p_fd;
		bal = p_fg.ht-p_fd.ht;
		ht = 1+java.lang.Math.max(p_fg.ht,p_fd.ht);
	}
	
	/**
	 * Initialisation par valeur, position, fils gauche et fils droit.
	 * @param p_val {@link #val}
	 * @param p_pos {@link #positionBloc}
	 * @param p_fg {@link #fg}
	 * @param p_fd {@link #fd}
	 */
	public Pair_Tree(int p_val, int p_pos, Pair_Tree p_fg, Pair_Tree p_fd){
		val = p_val;
		fg = p_fg;
		fd = p_fd;
		bal = p_fg.ht-p_fd.ht;
		ht = 1+java.lang.Math.max(p_fg.ht,p_fd.ht);
		positionBloc = p_pos;
	}
	
	/**
	 * Initialisation par valeur.
	 * @param p_val {@link #val}
	 */
	public Pair_Tree(int p_val) {
		this(p_val, new Pair_Tree(), new Pair_Tree());
	}
	
	/* -----------------------------------------------------------------
	 * Affichage(s)
	 * ----------------------------------------------------------------- 
	 */
	
	/**
	 * Vue simplifié
	 */
	public String toString() {
		return String.valueOf(val)+" ("+String.valueOf(bal)+") ";
	}
	
	/**
	 * Affichage Complet
	 */
	public void affichage() {
			System.out.println(" Valeur : " + this.val + ", Flag : " + this.estInversé + ", bal : " + this.bal + ", orienté : " + this.estOrienté + ", nbArcOrienté : " + this.nbArcOrienté + ", successeur : " + this.successeur + ", homologue : " + this.pos + ", POSCorrespondant : " + this.positionBloc);
		if(this.fg != null){
			this.fg.affichage();
		}
		if(this.fd != null){
			this.fd.affichage();
		}
	}
	
    /**
     * Accès à la position X dans le plan
     * @return voir {@link #x}
     */
    public int X() {
        return x;
    }
    
    /**
     * Accès à la position Y dans le plan
     * @return voir {@link #y}
     */
    public int Y() {
        return y;
    }
    
    /**
     * Modifie les valeurs x et y dans le plan 
     * @param x
     * @param y
     */
    public void fixerPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
	
	 /**
     * Calcul de la position d'un noeud dans le plan
     * return entier
     */
    public int calculerPositions(int xCourant, int yCourant) {
        /* on augmente xCourant afin d'accueillir toute la largeur du fils gauche */
        if (!(this.fg == null))
            xCourant = this.fg.calculerPositions(xCourant, yCourant + 1);
        
        /* la valeur de xCourant est maintenant l'abscisse de notre noeud */
        fixerPosition(xCourant, yCourant);
        xCourant = xCourant + 1;
        
        /* on deplace xCourant afin d'accueillir toute la largeur du fils droit */
        if (!(this.fd == null))
            xCourant = this.fd.calculerPositions(xCourant, yCourant + 1);
        
        /* la valeur de xCourant comprend maintenant la largeur de notre arbre */
        return xCourant;
    }
	
	/* -----------------------------------------------------------------
	 * Getter(s) / Setter(s)
	 * -----------------------------------------------------------------
	 */
    
	/**
	 * Getteur fils gauche
	 * @return {@link #fg}
	 */
	public Pair_Tree getFG() {
		return fg;
	}
	
	/**
	 * Getteur fils droit
	 * @return {@link #fd}
	 */
	public Pair_Tree getFD() {
		return fd;
	}
    
	/**
	 * Getteur de la position du correspondant
	 * @return {@link #positionBloc}
	 */
    public int getPositionBloc(){
    	return this.positionBloc;
    }
    
	/**
	 * Getteur de la valeur
	 * @return {@link #val}
	 */
    public int getVal(){
    	return this.val;
    }
	
	/**
	 * Getteur de l'homologue
	 * @return {@link #pos}
	 */
    public Search_Tree getST(){
    	return this.pos;
    }
    
	/**
	 * Getteur du successeur
	 * @return {@link #successeur}
	 */
	public Pair_Tree getSuccesseur(){
		return successeur;
	}
    
	/**
	 * Setteur de l'homologue Search Tree
	 * @param p_pos {@link #pos}
	 */
    public void setST(Search_Tree p_pos){
    	this.pos = p_pos;
    }
    
	/**
	 * Setteur de l'inversion du noeud
	 * @param p_estInversé {@link #estInversé}
	 */
	public void setEstInversé(boolean p_estInversé){
		estInversé = p_estInversé;
	}
    
	/**
	 * Setteur d'arc orienté du noeud
	 * @param p_estOrienté {@link #estOrienté}
	 */
    public void setEstOrienté(boolean p_estOrienté) {
    	this.estOrienté = p_estOrienté;
    }

    /* -----------------------------------------------------------------
	 * Méthode d'équilibrage
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * Rotation Gauche
	 * <p>voir {@link #Pair_Tree(int, int, Pair_Tree, Pair_Tree)}</p>
	 */
	public void rotationG() {
		if(fd.vide){
			System.out.println("Erreur - Rotation Droite");
			return ;
		 }
		 fg = new Pair_Tree(val, positionBloc, fg,fd.fg);
		 this.val = fd.val;
		 this.positionBloc = fd.positionBloc;
		 fd = fd.fd;
		 ht = 1+java.lang.Math.max(fg.ht,fd.ht);
		 bal = fg.ht-fd.ht; 
	}
	
	/**
	 * Rotation Droite
	 * <p>voir {@link #Pair_Tree(int, int, Pair_Tree, Pair_Tree)}</p>
	 */
	public void rotationD() {
		if(fg.vide){
			System.out.println("Erreur - Rotation Gauche");
			return ;
		 }
		 fd = new Pair_Tree(val, positionBloc,fg.fd,fd);
		 val = fg.val;
		 positionBloc = fg.positionBloc;
		 fg = fg.fg;
		 ht = 1+java.lang.Math.max(fg.ht,fd.ht);
		 bal = fg.ht-fd.ht;
	}
	
	/**
	 * Rotation Gauche-Droite
	 * <p>voir {@link #rotationG()}</p>
	 * <p>voir {@link #rotationD()}</p>
	 */
	public void rotationGD(){
		if(fg.vide){
			System.out.println("Erreur - Rotation Gauche-Droite 1");
			return ;
		}
		if(fg.fd.vide){
			System.out.println("Erreur - Rotation Gauche-Droite 2");
			return ;
		}
		fg.rotationG();
		rotationD();
	}
	
	/**
	 * Rotation Droite-Gauche
	 * <p>voir {@link #rotationD()}</p>
	 * <p>voir {@link #rotationG()}</p>
	 */
	public void rotationDG(){
		if(fd.vide){
			System.out.println("Erreur - Rotation Droite-Gauche 1");
			return ;
		}
		if (fd.fg.vide){
			 System.out.println("Erreur - Rotation Droite-Gauche 2");
			 return ;
		}
		fd.rotationD();
		rotationG();
	}
	
	/* -----------------------------------------------------------------
	 * Méthode(s)
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * Transforme un noeud en une feuille
	 * @param p_val {@link #ht}
	 * <p>voir {@link #Pair_Tree()}</p>
	 */
    public void Feuille(int p_val){ 
    	val=p_val;
    	vide=false; 
    	fg=new Pair_Tree();
    	fd=new Pair_Tree();
    	bal=0;
    	ht=1;
    }
    
	/**
	 * Transforme un noeud en une feuille
	 * @param p_val {@link #val}
	 * @param p_ht {@link #ht}
	 * <p>voir {@link #Pair_Tree()}</p>
	 */
    public void Feuille(int p_val, int p_ht){ 
    	val=p_val;
    	vide=false; 
    	fg=new Pair_Tree();
    	fd=new Pair_Tree();
    	bal=0;
    	ht=1;
    	positionBloc = p_ht;
    }
    
    /**
     * Ajoute un élément dans le Pair Tree
     * @param p_val {@link #val}
     * @param p_posD {@link #positionBloc}
     * <p>voir {@link #rotationD()}</p>
     * <p>voir {@link #rotationG()}</p>
     * <p>voir {@link #rotationDG()}</p>
     * <p>voir {@link #rotationGD()}</p>
     */
    public void ajouter(int p_val, int p_posD) {	
    	Pair_Tree pos = this;
    	Pair_Tree precedent = null; // référence sur le dernier noeud ayant une balance différent de 0.
    	
    	//Recherche de l'endroit ou ajouté le noeud
    	while(!(pos.vide)){
    		//System.out.println("positionBloc de la racine : " + this.positionBloc);
    		if(pos.bal != 0){
    			precedent=pos;
    		}
    		if(p_posD<=pos.positionBloc){
    			pos = pos.fg;
    		}
    		else{
    			pos = pos.fd;
    		}
    	}
   
    	pos.Feuille(p_val, p_posD); //Et on ajoute dans le Search Tree
    	Pair_Tree posx = pos; //On sauvegarde la position de l'élément ajouté
    	
    	if(precedent == null){
    		precedent=this; // tous les ancetres de l'élément ajouté ont une balance null.
    	}
    	
    	//On fait la mise a jours de la balance depuis la racine !
    	pos = precedent; // on fait la mise a jours des balances depuis la variable prec jusqu'a la feuille de l'élément ajouté
    	while(pos != posx){    
    		if(p_posD<=pos.positionBloc){ //On ajoute à gauche 
    			pos.bal++;
    			if(pos.bal > 0){ //On regarde si la balance du dernier noeud ayant une balance différente de 0 valait -1 avant l'ajout !
    				pos.ht++; 
    			}
    			pos = pos.fg;
    		}
    		else{ //On ajoute à droite
    			pos.bal--;
    			if(pos.bal < 0){ //On regarde si la balance du dernier noeud ayant une balance différente de 0 valait +1 avant l'ajout !
    				pos.ht++;
    			}
    			pos = pos.fd;
    		}
    	}
    	
    	//On fait l'équilibrage si nécessaire
    	switch(precedent.bal){
    		case -2 : Pair_Tree racined = precedent.fd; //Ajout à droite, donc prec.fd existe 
    	    	if(p_posD <= racined.positionBloc){
    	    		//System.out.println(posD);
    	    		//precedent.parcours();
    	    		precedent.rotationDG();
    	    	}
    	    	else{
    	    		//System.out.println("passe");
    	    		precedent.rotationG();
    	    	}
    	    	break;	
    		case -1 :
    		case 0 :
    		case 1 : break;
    		case 2 :  Pair_Tree racineg = precedent.fg; //ajout à gauche, donc prec.fg existe 
		    	if(p_posD <= racineg.positionBloc){
		    		precedent.rotationD();
		    	}
		    	else{
		    		precedent.rotationGD();
		    	}
		    	break;
    		default : System.out.println("erreur !");	
    	}
    	/*System.out.println("--------------------------------------------------------------------------------");
    	System.out.println();
    	this.parcours();
    	System.out.println();
    	System.out.println("--------------------------------------------------------------------------------");*/
     }
    
    /**
     * Recherche d'un element dans le Pair_Tree:
     * @param p_val {@link #val}
     * @param p_comparable {@link #positionBloc}
     * @return Pair Tree
     */
    public Pair_Tree rechercher(int p_val, int p_comparable) {
    	if(vide){
    		return null;
    	}
    	else{ 
    		if(val == p_val){
    			//System.out.println("Reussi");
    			return this;
    		}
    		else if(p_comparable <= this.positionBloc){
    			return fg.rechercher(p_val, p_comparable); 
    		}
    	    else{
    	    	return fd.rechercher(p_val, p_comparable);
    	    }
    	}
    }
        
    /**
     * Fonction qui associe les noeuds du Pair_tree avec leurs homologues dans le Search_Tree !
     * @param p_pos {@link #pos}}
     * @param p_liste {@link Lecture#getListe()}
     * <p>voir {@link #parcours()}</p>
     */
    public void liaison_ST(Search_Tree p_pos, ArrayList<Integer> p_liste){
    	if(!(vide)){
    		this.setST(p_pos.parcours(this.val, p_liste));
    		fg.liaison_ST(p_pos, p_liste);
    	    fd.liaison_ST(p_pos, p_liste);
    	}
    }
    
    /**
     * Fonction faisant la liaison entre les correspondants dans l'arbre Pair Tree
     * @param p_liste {@link Lecture#getListe()}
     * @param p_listeCorrespondant {@link Lecture#getListeCorrespondant()}
     * @param p_listePT {@link Lecture#getListePT()}
     */
    public void liaison(ArrayList<Integer> p_liste, ArrayList<Integer> p_listeCorrespondant, ArrayList<Pair_Tree> p_listePT){
    	if(!vide){
    		if(vide){
    			//System.out.println("Arbre Vide ! ");
    			return;
    		}
    		else{
    				if(this.getPositionBloc() + 1 == p_liste.size()){} // Permet d'éliminer le cas du dernier gênes n'ayant pas de succeseur.
    				else{
    					for(int cpt = 1; cpt < p_listePT.size(); cpt++){
    						if(p_listePT.get(cpt).rechercher(p_liste.get(this.getPositionBloc()), p_listeCorrespondant.get(this.getPositionBloc())) == null){}
    						else{
    							this.successeur = p_listePT.get(cpt).rechercher(p_liste.get(this.getPositionBloc()), p_listeCorrespondant.get(this.getPositionBloc()));
    							if(this.getVal() > 0){
    			    			if(this.successeur.getVal() > 0){
    			    				this.setEstOrienté(false);
    			    			}else{
    			    				this.setEstOrienté(true);
    			    			}
    			    		}else{
    			    			if(this.successeur.getVal() > 0){
    			    				this.setEstOrienté(true);
    			    			}else{
    			    				this.setEstOrienté(false);
    			    			}
    			    		}
    						}
    				}
    			}
    		}
    		fg.liaison(p_liste, p_listeCorrespondant, p_listePT);
    		fd.liaison(p_liste, p_listeCorrespondant, p_listePT);
    	}
    }
    
    /**
     * Détermine le nombre d'arc orienté par noeud de l'arbre
     * @return {@link #nbArcOrienté}
     */
    public int calculeArcOrienté(){
    	if(vide){
    		//System.out.println("PASSE");
    		return 0;
    	}
    	if(this.estOrienté == true){
    		return this.fg.calculeArcOrienté() + this.fd.calculeArcOrienté() + 1;
    	}else{
    		return this.fg.calculeArcOrienté() + this.fd.calculeArcOrienté();
    	}
    }
    
    /**
     * parcours calculant le nombre d'arc orienté par noeud de l'arbre ! 
     * <p>voir {@link #calculeArcOrienté()}</p>
     */
    public void parcoursArcOrienté() {
    	if(!(vide)){
    		//System.out.println(this.calculeArcOrienté());
    		this.nbArcOrienté = this.calculeArcOrienté();
    	    fg.parcoursArcOrienté();
    	    fd.parcoursArcOrienté();
    	}
    }
    
    /**
     * parcours préfixe du Pair_Tree
     */
    public void parcours() {
    	if(!(vide)){
    		System.out.print(this+", ");
    		System.out.print(this.positionBloc + " <- position , ");
    	    fg.parcours();
    	    fd.parcours();
    	}
    }	
    
    /**
     * Donne la hauteur du noeud 
     * @return {@link #ht}
     */
    public int hauteur(){
    	 if(vide){
    		 return 0;
    	 }
    	 else{
    		 return 1+java.lang.Math.max(fg.hauteur(),fd.hauteur()); 
    	 }
     }
    
    /**
     * Découpage du Pair Tree
     * @param p_pos {@link #positionBloc}
     * @param p_fg {@link #fg}
     * @param p_fd {@link #fd}
     * <p>voir {@link #getPositionBloc()}</p>
     * <p>voir {@link #ajouter(int, int)}</p>
     */
    public void découpe(int p_pos/*position du correspondant*/, Pair_Tree p_fg, Pair_Tree p_fd){
    	if(!(vide)){
    		System.out.println(this.pos.getPositionBloc());
    		this.pos.affichage();
    		System.out.println("===========================================================================");
    		if(p_pos > (this.pos.getPositionBloc())){
    			p_fd.ajouter(this.val, this.positionBloc);
    		}else{
    			p_fg.ajouter(this.val, this.positionBloc);
    		}
    		this.fg.découpe(p_pos, p_fg, p_fd);
    		this.fd.découpe(p_pos, p_fg, p_fd);
    	}
   }
    
    /**
     * Lecture inversé d'un noeud
     */
    public void lecture_inverse(){
    	if(vide){}
    	else{
    		this.estInversé = false;
    		this.val = -1*(this.val);
    		Pair_Tree inter = this.fg;
    		this.fg = this.fd;
    		this.fd = inter;
    		this.fg.lecture_inverse();
    		this.fd.lecture_inverse();
    	}
    } 
    
    /**
     * Regarde si le noeud possède un ou des arcs orientés dans ses fils et lui-même
     * @return booléan
     */
    public boolean possèdeArcOrienté(){
    	if(this.nbArcOrienté > 0)
    		return true;
    	else
    		return false;
    }
    
    /**
     * Retourne le Pair_Tree contenant un arc orienté
     * @return Pair Tree
     */
    public Pair_Tree estArcOrienté(){
    	if(this.estOrienté == true){
    		return this;
    	}else{
    		if(this.fg.nbArcOrienté != 0){
    			return this.fg.estArcOrienté();
    		}else{
    			return this.fd.estArcOrienté();
    		}
    	}
    }
}
