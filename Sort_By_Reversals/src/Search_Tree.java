import java.util.ArrayList;

/**
 * 
 * @author BRAULT Benjamin 
 * @version 0.1
 * @since 0.0
 */

public class Search_Tree {
	
	/* -----------------------------------------------------------------
	 * Attribut des Search Tree's
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * Valeur x pour l'affichage
	 */
	private int x;
	/**
	 * Valeur y pour l'affichage
	 */
	private int y;
	
	/**
	 * Valeur du noeud.
	 */
	private int val; 
	/**
	 * Position dans le bloc du début.
	 */
	private int positionBloc;
	/**
	 * Nombre de noeud en-dessous du noeud.
	 */
	private int nb_Tree;
	/**
	 * Flag définissant le sens de lecture du noeud et de ses fils.
	 */
	private boolean estInversé;  
	/**
	 * Fils gauche du noeud.
	 */
	private Search_Tree fg;
	/**
	 * Fils droit du noeud.
	 */
	private Search_Tree fd;
	/**
	 * Père du noeud.
	 */
	private Search_Tree pere;
	/**
	 * Flag définissant si le noeud est vide. 
	 */
	private boolean vide;
	/**
	 * Balance du noeud. 
	 */
	private int bal;
	/**
	 * Hauteur d'un noeud
	 */
	private int ht;
	/**
	 * Successeur vers l'arbre suivant du noeud racine.
	 */
	private Search_Tree succPere;
	/**
	 * Position de début du bloc.
	 */
	private int deb;
	/**
	 * Position de fin du bloc.
	 */
	private int fin;
	
	/* -----------------------------------------------------------------
	 * Constructeur(s) des Search_Tree's
	 * -----------------------------------------------------------------
	 */
		
	/**
	 * Initialisation par défaut
	 */
	public Search_Tree() {
		vide = true;
		fg = null;
		fd = null;
		pere = null;
		succPere = null;
		nb_Tree = 0;
		bal = 0;
		ht = 0;
	}
	
	/**
	 * Initialisation par valeur, sous-arbre gauche et sous-arbre droit
	 * @param p_val {@link #val}
	 * @param p_fg {@link #fg}
	 * @param p_fd {@link #fd}
	 */
	public Search_Tree(int p_val, Search_Tree p_fg, Search_Tree p_fd) {
		val = p_val;
		fg = p_fg;
		fd = p_fd;
		nb_Tree = 1;
		pere = null;
		succPere = null;
		bal = p_fg.ht-p_fd.ht;
		ht = 1+java.lang.Math.max(p_fg.ht,p_fd.ht);
	}
	
	/**
	 * Initialisation par sous-arbre gauche et sous-arbre droit
	 * @param p_fg {@link #fg}
	 * @param p_fd {@link #fd}
	 */
	public Search_Tree(Search_Tree p_fg, Search_Tree p_fd){
		fg = p_fg;
		fd = p_fd;
		bal = p_fg.ht-p_fd.ht;
		ht = 1+java.lang.Math.max(p_fg.ht,p_fd.ht);
	}
	
	/**
	 * Initialisation par valeur, position, sous-arbre gauche et sous-arbre droit
	 * @param p_val {@link #val}
	 * @param p_pos {@link #positionBloc}
	 * @param p_fg {@link #fg}
	 * @param p_fd {@link #fd}
	 */
	public Search_Tree(int p_val, int p_pos, Search_Tree p_fg, Search_Tree p_fd){
		val = p_val;
		fg = p_fg;
		fd = p_fd;
		nb_Tree = 1;
		pere = null;
		succPere = null;
		bal = p_fg.ht-p_fd.ht;
		ht = 1+java.lang.Math.max(p_fg.ht,p_fd.ht);
		setPositionBloc(p_pos);
	}
	
	/**
	 * Initialisation par valeur
	 * @param p_val {@link #val}
	 * <p>voir {@link #Search_Tree()}</p>
	 */
	public Search_Tree(int p_val) {
		this(p_val, new Search_Tree(), new Search_Tree());
	}
	
	/* -----------------------------------------------------------------
	 * Getter(s) et Setter(s)
	 * ----------------------------------------------------------------- 
	 */
	

	/**
	 * Getteur fils gauche
	 * @return {@link #fg}
	 */
	public Search_Tree getFG() {
		return fg;
	}
	
	/**
	 * Getteur fils droit
	 * @return {@link #fd}
	 */
	public Search_Tree getFD() {
		return fd;
	}
	
	/**
	 * Getteur du pere
	 * @return {@link #pere}
	 */
	public Search_Tree getPere() {
		return pere;
	}
	
	/**
	 * Getteur de la valeur
	 * @return {@link #val}
	 */
	public int getVal(){
		return val;
	}
	
	/**
	 * Getteur de la position
	 * @return {@link #positionBloc}
	 */
	public int getPositionBloc() {
		return positionBloc;
	}
	
	/**
	 * Getteur du successeur du père
	 * @return {@link #succPere}
	 */
	public Search_Tree getSuccPere() {
		return succPere;
	}
	
	/**
	 * Getteur de début de bloc
	 * @return {@link #deb}
	 */
	public int getDeb() {
		return deb;
	}
	
	/**
	 * Getteur de fin de bloc
	 * @return {@link #fin}
	 */
	public int getFin() {
		return fin;
	}
	
	/**
	 * Setteur du pere
	 * @param p_ST {@link #pere}
	 */
	public void setPere(Search_Tree p_ST) {
		pere = p_ST;
	}
	
	/**
	 * Setteur de l'inversion du noeud
	 * @param p_inversé {@link #estInversé}
	 */
	public void setEstInversé(boolean p_inversé){
		estInversé = p_inversé;
	}
	
	/**
	 * Setteur de la position
	 * @param p_positionBloc {@link #positionBloc}
	 */
	public void setPositionBloc(int p_positionBloc) {
		this.positionBloc = p_positionBloc;
	}
	
	/**
	 * Setteur du successeur père
	 * @param p_succPere {@link #succPere}
	 */
	public void setSuccPere(Search_Tree p_succPere) {
		this.succPere = p_succPere;
	}

	/**
	 * Setteur de début de bloc
	 * @param p_deb {@link #deb}
	 */
	public void setDeb(int p_deb) {
		this.deb = p_deb;
	}

	/**
	 * Setteur de fin de bloc
	 * @param p_fin {@link #fin}
	 */
	public void setFin(int p_fin) {
		this.fin = p_fin;
	}
	
	/* -----------------------------------------------------------------
	 * Affichage(s)
	 * ----------------------------------------------------------------- 
	 */
	
	/**
	 * Vue simplifié
	 */
	public String toString() {
		return String.valueOf(val)+" ("+String.valueOf(bal)+") ";
	}
	
	/**
	 * Affichage Complet
	 */
	public void affichage() {
		if (this.succPere != null){
			System.out.println(" Valeur : " + this.val + ", Nb_SA : " + this.nb_Tree + ", estInversé : " + this.estInversé + ", Noeud père successeur : " + this.succPere + "bal : " + this.bal + ", POS : " + this.getPositionBloc());
		}
		else if (this.pere == null){
			System.out.println(" Valeur : " + this.val + ", Nb_SA : " + this.nb_Tree + ", estInversé : " + this.estInversé + ", Noeud père : null, " + "bal : " + this.bal + ", POS : " + this.getPositionBloc());
		}else{
			System.out.println(" Valeur : " + this.val + ", Nb_SA : " + this.nb_Tree + ", estInversé : " + this.estInversé + ", Noeud père : " + this.pere.val + ", bal : " + this.bal + ", POS : " + this.getPositionBloc());
		}
		if(this.fg != null){
			this.fg.affichage();
		}
		if(this.fd != null){
			this.fd.affichage();
		}
	}
	
    /**
     * Accès à la position X dans le plan
     * @return voir {@link #x}
     */
    public int X() {
        return x;
    }
    
    /**
     * Accès à la position Y dans le plan
     * @return voir {@link #y}
     */
    public int Y() {
        return y;
    }
    
    /**
     * Modifie les valeurs x et y dans le plan 
     * @param x
     * @param y
     */
    public void fixerPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
	
	 /**
     * Calcul de la position d'un noeud dans le plan
     * return entier
     */
    public int calculerPositions(int xCourant, int yCourant) {
        /* on augmente xCourant afin d'accueillir toute la largeur du fils gauche */
        if (!(this.fg == null))
            xCourant = this.fg.calculerPositions(xCourant, yCourant + 1);
        
        /* la valeur de xCourant est maintenant l'abscisse de notre noeud */
        fixerPosition(xCourant, yCourant);
        xCourant = xCourant + 1;
        
        /* on deplace xCourant afin d'accueillir toute la largeur du fils droit */
        if (!(this.fd == null))
            xCourant = this.fd.calculerPositions(xCourant, yCourant + 1);
        
        /* la valeur de xCourant comprend maintenant la largeur de notre arbre */
        return xCourant;
    }
	
	/* -----------------------------------------------------------------
	 * Méthode d'équilibrage
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * Rotation Gauche
	 * <p>voir {@link #Search_Tree(int, int, Search_Tree, Search_Tree)}</p>
	 * <p>voir {@link #setPositionBloc(int)}</p>
	 * <p>voir {@link #getPositionBloc()}</p>
	 */
	public void rotationG() {
		if(fd.vide){
			 System.out.println("Erreur - Rotation Droite");
			 return ;
		 }
		 Search_Tree inter = this.pere;
		 fg = new Search_Tree(val, getPositionBloc(),fg,fd.fg);
		 this.fg.fg.pere = this.fg;
		 this.fg.fd.pere = this.fg;
		 this.val = fd.val;
		 this.setPositionBloc(fd.getPositionBloc());
		 this.fd = fd.fd;
		 this.fd.fd.pere = this.fd;
		 this.fd.fg.pere = this.fd;
		 fd.pere = this;
		 fg.pere = this;
		 this.pere = inter;
		 ht = 1+java.lang.Math.max(fg.ht,fd.ht);
		 bal = fg.ht-fd.ht;
	}
	
	/**
	 * Rotation Droite
	 * <p>voir {@link #Search_Tree(int, int, Search_Tree, Search_Tree)}</p>
	 * <p>voir {@link #setPositionBloc(int)}</p>
	 * <p>voir {@link #getPositionBloc()}</p>
	 */
	public void rotationD(){
		if(fg.vide){
			 System.out.println("Erreur - Rotation Gauche");
			 return ;
		 }
		 Search_Tree inter = this.pere;
		 fd = new Search_Tree(val, getPositionBloc(), fg.fd,fd);
		 val = fg.val;
		 setPositionBloc(fg.getPositionBloc());
		 fg = fg.fg;
		 fg.pere = this;
		 fd.pere = this;
		 this.pere = inter;
		 ht = 1+java.lang.Math.max(fg.ht,fd.ht);
		 bal = fg.ht-fd.ht;
	}
	
	/**
	 * Rotation Gauche-Droite
	 * <p>voir {@link #rotationG()}</p>
	 * <p>voir {@link #rotationD()}</p>
	 */
	public void rotationGD() {
		if (fg.vide) {
			 System.out.println("Erreur - Rotation Gauche-Droite 1");
			 return ;}
		if (fg.fd.vide) {
			 System.out.println("Erreur - Rotation Gauche-Droite 2");
			 return ;}
		fg.rotationG();
		rotationD();
	}
	
	/**
	 * Rotation Droite-Gauche
	 * <p>voir {@link #rotationD()}</p>
	 * <p>voir {@link #rotationG()}</p>
	 */
	public void rotationDG() {
		if (fd.vide) {
			 System.out.println("Erreur - Rotation Droite-Gauche 1");
			 return ;}
		if (fd.fg.vide) {
			 System.out.println("Erreur - Rotation Droite-Gauche 2");
			 return ;}
		fd.rotationD();
		rotationG();
	}
	
	/* -----------------------------------------------------------------
	 * Méthode(s)
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * Transforme un noeud en feuille
	 * @param p_val {@link #val}
	 * <p>voir {@link #Search_Tree()}</p>
	 */
    public void feuille(int p_val) {
    	val=p_val;
    	vide=false;
    	fg=new Search_Tree();
    	fd=new Search_Tree();
    	bal=0;
    	ht=1;
    }
	
	/**
	 * Transforme un noeud en une feuille
	 * @param p_val {@link #val}
	 * @param p_ht {@link #ht}
	 * <p>voir {@link #Search_Tree()}</p>
	 * <p>voir {@link #setPositionBloc(int)}</p>
	 */
    public void feuille(int p_val, int p_ht) {
    	val=p_val;
    	vide=false;
    	fg=new Search_Tree();
    	fg.pere = this;
    	fd=new Search_Tree();
    	fd.pere = this;
    	bal=0;
    	ht=1;
    	setPositionBloc(p_ht);
    }
	
    /**
     * Ajoute un élément dans le Search Tree
     * @param p_val {@link #val}
     * @param p_posD {@link #positionBloc}
     * <p>voir {@link #getPositionBloc()}</p>
     * <p>voir {@link #feuille(int, int)}</p>
     * <p>voir {@link #rotationD()}</p>
     * <p>voir {@link #rotationG()}</p>
     * <p>voir {@link #rotationDG()}</p>
     * <p>voir {@link #rotationGD()}</p>
     * <p>voir {@link #parcours_Calcul()}</p>
     */
    public void ajouter(int p_val, int p_posD) {	
    	Search_Tree pos = this;
    	Search_Tree posPrec = null; //référence du parent sur le noeud ajouté
    	Search_Tree precedent = null; // référence sur le dernier noeud ayant une balance différent de 0.
    	
    	//Recherche de l'endroit ou ajouté le noeud
    	while(!(pos.vide)){
    		if(pos.bal != 0){
    			precedent=pos;
    		}
    		if(p_posD<=pos.getPositionBloc()){
    			posPrec=pos;
    			pos = pos.fg;
    		}
    		else{
    			posPrec = pos;
    			pos = pos.fd;
    		}
    	}
    	
    	pos.pere = posPrec; //On définit le pere
    	pos.feuille(p_val, p_posD); //Et on ajoute dans le Search Tree  //On définit la position de l'élément dans le bloc 
    	Search_Tree posx = pos; //On sauvegarde la position de l'élément ajouté
    	posx.pere = posPrec; //Idem pour le pere de l'élément
    	
    	if(precedent == null){
    		precedent=this; // tous les ancetres de l'élément ajouté ont une balance null.
    	}
    	
    	//On fait la mise a jours de la balance depuis la racine !
    	pos = precedent; // on fait la mise a jours des balances depuis la variable prec jusqu'a la feuille de l'élément ajouté
    	while(pos != posx){    
    		if(p_posD<=pos.getPositionBloc()){ //On ajoute à gauche 
    			pos.bal++;
    			if(pos.bal > 0){ //On regarde si la balance du dernier noeud ayant une balance différente de 0 valait -1 avant l'ajout !
    				pos.ht++; 
    			}
    			pos = pos.fg;
    		}
    		else{ //On ajoute à droite
    			pos.bal--;
    			if(pos.bal < 0){ //On regarde si la balance du dernier noeud ayant une balance différente de 0 valait +1 avant l'ajout !
    				pos.ht++;
    			}
    			pos = pos.fd;
    		}
    	}
    	
    	//On fait l'équilibrage si nécessaire
    	switch(precedent.bal){
    		case -2 : Search_Tree racined = precedent.fd; //Ajout à droite, donc prec.fd existe 
    	    	if(p_posD <= racined.getPositionBloc()){
    	    		precedent.rotationDG();
    	    	}
    	    	else{
    	    		precedent.rotationG();
    	    	}
    	    	break;	
    		case -1 :
    		case 0 :
    		case 1 : break;
    		case 2 :  Search_Tree racineg = precedent.fg; //ajout à gauche, donc prec.fg existe 
		    	if(p_posD <= racineg.getPositionBloc()){
		    		precedent.rotationD();
		    	}
		    	else{
		    		precedent.rotationGD();
		    	}
		    	break;
    		default : System.out.println("erreur !");	
    	}
    	this.parcours_Calcul(); //On met à jours les sous arbres dans chaque noeud !
     }
	
    /**
     * Calcul le nombre de sous arbres d'un noeud !
     * @return {@link #nb_Tree}
     */
    public int calcul_nbSA() {
    	if(vide){
    		return 0;
    	}else{
    		return this.fg.calcul_nbSA() + this.fd.calcul_nbSA() + 1;  
    	}
    }
    
    /**
     * Recherche d'un element dans un AVL
     * @param p_val {@link #val}
     * @param p_comparable {@link #positionBloc}
     * @return Search Tree
     * <p>voir {@link #getPositionBloc()}</p>
     */
    public Search_Tree rechercheST(int p_val, int p_comparable) {
    	if(vide){
    		return null;
    	}
    	else{ 
    		if(val == p_val){
    			//System.out.println("Reussi");
    			return this;
    		}
    		else if(p_comparable <= this.getPositionBloc()){
    			return fg.rechercheST(p_val, p_comparable); 
    		}
    	    else{
    	    	return fd.rechercheST(p_val, p_comparable);
    	    }
    	}
    }
    /**
     * Lecture inversé d'un noeud
     */
    public void lecture_inverse(){
    	if(vide){}
    	else{
    		this.estInversé = false;
    		this.val = -1*(this.val);
    		Search_Tree inter = this.fg;
    		this.fg = this.fd;
    		this.fd = inter;
    		this.fg.lecture_inverse();
    		this.fd.lecture_inverse();
    	}
    }
    
   
    /**
     * parcours préfixe du Pair_Tree
     * <p>{@link #getPositionBloc()}</p>
     */
    public void parcours() {
    	if(!(vide)){
    		System.out.print(this+", ");
    		System.out.print(this.getPositionBloc()+" <- position , ");
    	    fg.parcours();
    	    fd.parcours();
    	}
    }	
   
   /**
    * Calcul la position du noeud
    * @return {@link #positionBloc}
    */
   public int position(){
	   Search_Tree pt = this;
	   Search_Tree pt2 = this;
	   while(pt2.succPere == null){
		   pt2 = pt2.pere;
	   }
	   pt2.positionBloc = (pt2.fg.nb_Tree + 1) + pt2.deb - 1;
	   int w = pt2.positionBloc;
	   while(pt2.val != pt.val){
		   if(pt.positionBloc > pt2.positionBloc){
			   pt2 = pt2.fd;
			   w = w + pt2.fg.nb_Tree + 1;
		   }else{
			   pt2 = pt2.fg;
			   w = w - pt2.fd.nb_Tree - 1;
		   }
	   }
	   return w;
   }
    
   /**
    * Coupure de l'arbre en O(n²) (pas bien mais la fonction en O(log n) ne marche pas)
    * @param p_pos {@link #positionBloc}
    * @param p_fg {@link #fg}
    * @param p_fd {@link #fd}
    * <p>voir {@link #getPositionBloc()}</p>
    */
   public void découpe(int p_pos, Search_Tree p_fg, Search_Tree p_fd){
    	if(!(vide)){
    		if(p_pos <= this.getPositionBloc()){
    			p_fd.ajouter(this.val, this.getPositionBloc());
    		}else{
    			p_fg.ajouter(this.val, this.getPositionBloc());
    		}
    		this.fg.découpe(p_pos, p_fg, p_fd);
    		this.fd.découpe(p_pos, p_fg,p_fd);
    	}
    }
   
   /**
    * Trouve le noeud racine de l'arbre 
    * @return Search Tree
    */
   public Search_Tree arbre(){
  	    Search_Tree pt2 = this;
    	while(pt2.succPere == null){
 		   pt2 = pt2.pere;
 	   }
    return pt2;
   }
    
    /**
     * parcours de l'arbre en calculant les sous arbres
     * <p>voir {@link #calcul_nbSA()}</p>
     */
    public void parcours_Calcul() {
    	if(! vide){
    		nb_Tree = calcul_nbSA();
    	    fg.parcours_Calcul();
    	    fd.parcours_Calcul();
    	}
    }
    
    /**
     * parcours de l'arbre en calculant les positions
     * <p>voir {@link #setPositionBloc(int)}</p>
     * <p>voir {@link #position()}</p>
     */
    public void parcours_calcul_POS() {
    	if(! vide){
    		setPositionBloc(position());
    	    fg.parcours_calcul_POS();
    	    fd.parcours_calcul_POS();
    	}
    }
    
    /**
     * Fusion partielle de deux arbres : O(log(n))
     * @param p_ST
     * @return Search Tree
     * <p>voir {@link #hauteur()}</p>
     * <p>voir {@link #Search_Tree(Search_Tree, Search_Tree)}</p>
     */
    public Search_Tree fusion(Search_Tree p_ST){
    	if(p_ST.vide){
    		return this;
    	}
    	if(this.vide){
    		return p_ST;
    	}
    	if((p_ST.hauteur() - this.hauteur() >= -1) && (p_ST.hauteur() - this.hauteur() <= 1)){
    		return new Search_Tree(p_ST, this);
    	}
    	if(p_ST.hauteur() > this.hauteur()){
    		if(p_ST.fd.hauteur() <= p_ST.fg.hauteur()){
    			return new Search_Tree(p_ST.fg, this.fusion(p_ST.fd));
    		}else if((p_ST.fd.fg.hauteur() > p_ST.fd.fd.hauteur()) && (p_ST.fd.fg.hauteur() > this.hauteur())){
    			return new Search_Tree(p_ST.fg, new Search_Tree(p_ST.fd.fg, this.fusion(p_ST.fd.fd)));
    		}else{
    			return new Search_Tree(new Search_Tree(p_ST.fg, p_ST.fd.fg), this.fusion(p_ST.fd.fd));
    		}
    	}else{
    		if(this.fg.hauteur() <= this.fd.hauteur()){
    			return new Search_Tree(this.fg.fusion(p_ST) ,this.fd);
    		}else if((this.fg.fd.hauteur() > this.fg.fg.hauteur()) && (this.fg.fd.hauteur() > p_ST.hauteur())){
    			return new Search_Tree(new Search_Tree(this.fg.fg.fusion(p_ST), this.fg.fd), this.fd);
    		}else{
    			return new Search_Tree(this.fg.fg.fusion(p_ST), new Search_Tree(this.fg.fd, this.fg));
    		}
    	}
    }
    
    /**
     * Cherche le plus grand élément du sous-arbre gauche
     * @return Search Tree
     */
    public Search_Tree maxTreeFG(){
    	if(vide){
    		return this.pere;
    	}
    	else{
    		this.nb_Tree = this.nb_Tree - 1;
    		return this.fd.maxTreeFG();
    	}
    }
    
    /**
     * Remonte à la racine le fils le plus grand de l'arbre contenant les plus petits éléments et concaténant le second arbre au fils droit à la nouvelle racine
     * @return Search Tree
     * <p>voir {@link #maxTreeFG()}</p>
     * <p>voir {@link #Search_Tree()}</p>
     */
    public Search_Tree merge2root(){
    	Search_Tree inter = this.fd.maxTreeFG();
    	//this.affichage();
    	this.fg.pere = this;
    	this.fd.pere = this;
    	if(inter.pere == this){
    		inter.fd = this.fg;
    		inter.fd.pere = inter;
    	}else{
    		if(inter.fg.vide){
    			inter.pere.fd = new Search_Tree();
    		}else{
    			inter.fg.pere = inter.pere;
    			inter.pere.fd = inter.fg;
    		}
    		inter.fg = this.fd;
    		inter.fd = this.fg;
    		inter.fg.pere = inter;
    		inter.fd.pere = inter;
    	}
    	inter.pere = null;
    	return inter;
    }
    
    /**
     * Fusion complète de deux arbres O(log n)
     * @param p_ST
     * @return Search Tree
     * <p>voir {@link #fusion(Search_Tree)}</p>
     * <p>voir {@link #merge2root()}</p>
     * <p>voir {@link #parcours_Calcul()}</p>
     */
    public Search_Tree merge(Search_Tree p_ST){
    	Search_Tree a = this.fusion(p_ST);
    	Search_Tree res = a.merge2root();
    	res.parcours_Calcul();
    	return res;
    }
    
    /**
     * Cherche l'élément dans l'arbre
     * @param p_val {@link #val}
     * @param p_comparable {@link #positionBloc}
     * @return Search Tree
     * <p>voir {@link #getPositionBloc()}</p>
     */
    public Search_Tree rechercher(int p_val, int p_comparable) {
    	if(vide){
    		return null;
    	}
    	else{ 
    		if(this.val == p_val){
    			//System.out.println("Reussi");
    			return this;
    		}
    		else if(p_comparable <= this.getPositionBloc()){
    			return fg.rechercher(p_val, p_comparable); 
    		}
    	    else{
    	    	return fd.rechercher(p_val, p_comparable);
    	    }
    	}
    }
    
    /**
     * parcours le Search Tree pour trouver l'élément p_val
     * @param p_val {@link #val}
     * @param p_liste {@link Lecture#getListe()}
     * @return Search_Tree
     */
    public Search_Tree parcours(int p_val, ArrayList<Integer> p_liste) {
    	int res = 0;
    	for(int cpt = 0; cpt < p_liste.size(); cpt++){
    		if(p_val == p_liste.get(cpt)){
    			res = cpt;
    		}
    	}
    	return rechercher(p_val, res);
    }
     
    /**
     * Donne la hauteur du noeud 
     * @return {@link #ht}
     */
    public int hauteur() {
    	if(vide){
    		return 0;
    	}else{
    		return 1+java.lang.Math.max(fg.hauteur(),fd.hauteur()); 
    	}
     }

}
