//import statements
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;

/**
 * 
 * @author BRAULT Benjamin 
 * @version 0.1
 * @since 0.0
 */

/**
 * 
 * Classe principale 
 *
 */
public class Algorithme {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		//*********************************************************************************************************************************************
		//*********************************************************************************************************************************************
		// /!\ ATTENTION : Pour voir le résultat des arbres en mode graphique à chaque étape : enlever en commantaire la ou les lignes 'setvisible(true) !' 
		//*********************************************************************************************************************************************
		//*********************************************************************************************************************************************

		
		
		//Lecture application = new Lecture();
		//application.visualisation();
		//System.out.println(l.getListeST().get(2).getDeb());
		//l.getListePT().get(1).affichage();
		System.out.println("----------------Test Search_Tree----------------");
		System.out.println();
		
		//Création du Search_Tree p_1 pour les tests (voir exemple index pour plus d'information)
		Search_Tree s_1 = new Search_Tree();
		s_1.ajouter(0, 0);
		s_1.ajouter(16, 1);
		s_1.ajouter(-13, 2);
		s_1.ajouter(9, 3);
		s_1.ajouter(3, 4);
		s_1.ajouter(-5, 5);
		s_1.ajouter(1, 6);
		s_1.ajouter(-8, 7);
		s_1.ajouter(15, 8);
		Panneau panneau = new Panneau(s_1);
		JFrame cadre = new JFrame("Search_Tree 1");
		cadre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre.setBounds(100, 100, 600, 400);
		cadre.setContentPane(panneau);
		//cadre.setVisible(true);
		s_1.affichage();
		
		
		Search_Tree s_2 = new Search_Tree();
		s_2.ajouter(10, 9);
		s_2.ajouter(-2, 10);
		s_2.ajouter(-6, 11);
		s_2.ajouter(-12, 12);
		s_2.ajouter(14, 13);
		s_2.ajouter(-17, 14);
		s_2.ajouter(4, 15);
		s_2.ajouter(7, 16);
		s_2.ajouter(-11, 17);
		System.out.println();
		s_2.affichage();
		
		Panneau panneau2 = new Panneau(s_2);
		JFrame cadre2 = new JFrame("Search_Tree 2");
		cadre2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre2.setBounds(100, 100, 600, 400);
		cadre2.setContentPane(panneau2);
		//cadre2.setVisible(true);
		
		s_2.setDeb(8);
		//s_2.fg.positionv2();
		s_2.affichage();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("----------------Affichage Search_Tree après découpe----------------");
		System.out.println();
		
				
		//Test de la découpe
		Search_Tree sc_1 = new Search_Tree();
		Search_Tree sc_2 = new Search_Tree();
		
		Search_Tree sc_3 = new Search_Tree();
		Search_Tree sc_4 = new Search_Tree();
		
		s_1.découpe(2/*position de l'élémént à découper*/, sc_1, sc_2);
		s_2.découpe(13/*position de l'élémént à découper*/, sc_3, sc_4);
		
		System.out.println("----------------Affichage Search_Tree n°1 obtenu après découpe----------------");
		System.out.println();
		sc_1.affichage();
		
		Panneau panneau3 = new Panneau(sc_1);
		JFrame cadre3 = new JFrame("Search_Tree n°1 obtenu après découpe de l'arbre 1");
		cadre3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre3.setBounds(100, 100, 600, 400);
		cadre3.setContentPane(panneau3);
		//cadre3.setVisible(true);
		
		System.out.println();
		System.out.println("----------------Affichage Search_Tree n°2 obtenu après découpe----------------");
		System.out.println();
		sc_2.affichage();
		Panneau panneau4 = new Panneau(sc_2);
		JFrame cadre4 = new JFrame("Search_Tree n°2 obtenu après découpe de l'arbre 1");
		cadre4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre4.setBounds(100, 100, 600, 400);
		cadre4.setContentPane(panneau4);
		//cadre4.setVisible(true);
		System.out.println("----------------Affichage Search_Tree n°3 obtenu après découpe----------------");
		System.out.println();
		sc_3.affichage();
		Panneau panneau5 = new Panneau(sc_3);
		JFrame cadre5 = new JFrame("Search_Tree n°1 obtenu après découpe de l'arbre 2");
		cadre5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre5.setBounds(100, 100, 600, 400);
		cadre5.setContentPane(panneau5);
		//cadre5.setVisible(true);
		System.out.println();
		System.out.println("----------------Affichage Search_Tree n°4 obtenu après découpe----------------");
		System.out.println();
		sc_4.affichage();
		Panneau panneau6 = new Panneau(sc_4);
		JFrame cadre6 = new JFrame("Search_Tree n°2 obtenu après découpe de l'arbre 2");
		cadre6.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre6.setBounds(100, 100, 600, 400);
		cadre6.setContentPane(panneau6);
		//cadre6.setVisible(true);
		System.out.println("----------------Test de la fonction d'inversion sur l'arbre----------------");
		System.out.println();
		sc_3.setEstInversé(true);
		sc_2.setEstInversé(true);
		System.out.println();
		System.out.println("----------------Affichage Search_Tree n°2 après inversion(s)----------------");
		System.out.println();
		sc_3.lecture_inverse();
		sc_3.affichage();
		Panneau panneau7 = new Panneau(sc_3);
		JFrame cadre7 = new JFrame("Search_Tree n°1 de l'arbre 2 après découpe + inversion");
		cadre7.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre7.setBounds(100, 100, 600, 400);
		cadre7.setContentPane(panneau7);
		//cadre7.setVisible(true);
		System.out.println("----------------Affichage Search_Tree n°3 après inversion(s)----------------");
		System.out.println();
		sc_2.lecture_inverse();
		sc_2.affichage();
		Panneau panneau8 = new Panneau(sc_2);
		JFrame cadre8 = new JFrame("Search_Tree n°2 de l'arbre 1 après découpe + inversion");
		cadre8.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre8.setBounds(100, 100, 600, 400);
		cadre8.setContentPane(panneau8);
		//cadre.setVisible(true);
		System.out.println("----------------Affichage Search_Tree après fusion----------------");
		System.out.println();
		System.out.println("----------------Affichage fusion de l'arbre n°1----------------");
		System.out.println();
		Search_Tree res = sc_1.merge(sc_3);
		res.affichage();
		Panneau panneau9 = new Panneau(res);
		JFrame cadre9 = new JFrame("Search_Tree n°1 après fusion de l'arbre n°1 de l'arbre 1 et l'arbre n°1 de l'arbre 2");
		cadre9.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre9.setBounds(100, 100, 600, 400);
		cadre9.setContentPane(panneau9);
		//cadre9.setVisible(true);
		System.out.println();
		System.out.println("----------------Affichage fusion de l'arbre n°3----------------");
		System.out.println();
		Search_Tree res2 = sc_2.merge(sc_4);
		res2.affichage();
		Panneau panneau10 = new Panneau(res2);
		JFrame cadre10 = new JFrame("Search_Tree n°2 après fusion de l'arbre n°2 de l'arbre 1 et l'arbre n°2 de l'arbre 2");
		cadre10.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre10.setBounds(100, 100, 600, 400);
		cadre10.setContentPane(panneau10);
		//cadre10.setVisible(true);
		System.out.println();
		System.out.println("----------------Test de la fonction recalcul POS----------------");
		System.out.println();
		res.setSuccPere(res2);
		//System.out.println(res.fd.fg.fd.positionv2());
		//.out.println(res.getFin());
		//System.out.println(res.positionv2());
		//res.fd.fd.affichage();
		//sc_2.affichage();
		//Test de la fusion
		
		System.out.println("----------------Test Pair_Tree----------------");
		
		//Création du Pair_tree p_1 pour les tests (voir exemple index pour plus d'information)
		
		System.out.println("----------------Affichage Pair_Tree----------------");
		Pair_Tree p_1 = new Pair_Tree();
		p_1.ajouter(0, 6);
		p_1.ajouter(16, 14);
		p_1.ajouter(-13, 13);
		p_1.ajouter(9, 7);
		p_1.ajouter(3, 15);
		p_1.ajouter(-5, 11);
		p_1.ajouter(1, 10);
		p_1.ajouter(-8, 3);
		p_1.ajouter(15, 1);
		p_1.affichage();
		Panneau2 panneau11 = new Panneau2(p_1);
		JFrame cadre11 = new JFrame("Pair_Tree 1");
		cadre11.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre11.setBounds(100, 100, 600, 400);
		cadre11.setContentPane(panneau11);
		//cadre11.setVisible(true);
		
		//******************************************************************************************************************************
		// /!\ Problème découpage Pair_Tree, du coup ne marche pas !
		//******************************************************************************************************************************
		
		
		/*
		System.out.println("----------------Affichage Pair_Tree après découpe----------------");
		System.out.println();
		//Test de la découpe
		Pair_Tree pc_1 = new Pair_Tree();
		Pair_Tree pc_2 = new Pair_Tree();
		p_1.découpe(13/*position du correspondant, pc_1, pc_2);
		System.out.println("----------------Affichage Pair_Tree n°1 obtenu après découpe----------------");
		System.out.println();
		pc_1.affichage();
		Panneau2 panneau12 = new Panneau2(pc_1);
		JFrame cadre12 = new JFrame("Affichage Pair_Tree n°1 obtenu après découpe");
		cadre12.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cadre12.setBounds(100, 100, 600, 400);
		cadre12.setContentPane(panneau12);
		cadre12.setVisible(true);
		System.out.println();
		System.out.println("----------------Affichage Pair_Tree n°2 obtenu après découpe----------------");
		System.out.println();
		pc_2.affichage();
		System.out.println("----------------Test de la fonction d'inversion sur l'arbre----------------");
		System.out.println();
		pc_1.setEstInversé(true);
		pc_2.setEstInversé(true);
		System.out.println();
		System.out.println("----------------Affichage Pair_Tree n°2 après inversion(s)----------------");
		System.out.println();
		//pc_1.lecture_inverse();
		//pc_2.affichage();
		
		//D.affichage();
		//s.Parcours();
		
		
		/*Pair_Tree pc_1 = new Pair_Tree();
		Pair_Tree pc_2 = new Pair_Tree();
		System.out.println("===================================================================================================================");
		//System.out.println(l.getListeST().get(1).position(l.getListeST().get(1).fd.fd));
		l.getListePT().get(1).découpe(2, pc_1, pc_2);
		pc_1.setEstInversé(true);
		pc_2.setEstInversé(true);
		pc_1.lecture_inverse();
		pc_2.lecture_inverse();
		pc_1.affichage();
		//l.getListePT().get(1).affichage();
		//System.out.println();
		//pc_1.affichage();*/
		
		}
	
	
	
}
