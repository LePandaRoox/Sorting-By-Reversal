//import statements
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * @author BRAULT Benjamin 
 * @version 0.1
 * @since 0.0
 */

/**
 * 
 * Classe permettant la lecture de la séquence pour effectuer le tri par inversions
 *
 */
public class Lecture {
	
	/** -----------------------------------------------------------------
	 *  Attribut(s)
	 *  -----------------------------------------------------------------
	 */

	/**
	 * Nom du fichier
	 */
	private String file = "/home/lepandaroux/1.txt" ; 
	/**
	 * Instanciation du fichier
	 */
	private java.io.File fichier = new java.io.File(file);
	/**
	 * Itérateur du fichier
	 */
	private java.util.Scanner lecteur = new java.util.Scanner(fichier);
	/**
	 * Liste des éléments de la séquence
	 */
	private ArrayList<Integer> liste = new ArrayList<Integer>();
	/**
	 * Liste de la position des correspondants
	 */
	private ArrayList<Integer> listeCorrespondant = new ArrayList<Integer>();
	/**
	 * Liste des Search Tree's
	 */
	private ArrayList<Search_Tree> listeST = new ArrayList<Search_Tree>();
	/**
	 * Liste des Pair Tree's
	 */
	private ArrayList<Pair_Tree> listePT = new ArrayList<Pair_Tree>();
	
	/* -----------------------------------------------------------------
	 * Getter(s) / Setter(s)
	 * -----------------------------------------------------------------
	*/
	
	/**
	 * Getteur de listeST
	 * @return listeST
	 */
	public ArrayList<Search_Tree> getListeST(){
		return listeST;
	}
	
	/**
	 * Getteur de listePT
	 * @return listePT
	 */
	public ArrayList<Pair_Tree> getListePT(){
		return listePT;
	}
	
	/**
	 * Getteur de liste
	 * @return liste
	 */
	public ArrayList<Integer> getListe(){
		return liste;
	}
	/**
	 * Getteur de listeCorrespondant
	 * @return listeCorrespondant
	 */
	public ArrayList<Integer> getListeCorrespondant(){
		return listeCorrespondant;
	}
	
	/* -----------------------------------------------------------------
	 * Affichage(s)
	 * ----------------------------------------------------------------- 
	 */
	
	/**
	 * Affiche la liste des correspondants
	 */
	public void affichage_liste(){
		for(int i=0; i < listeCorrespondant.size(); i++){
			System.out.println(listeCorrespondant.get(i));
		}
	}
	
	/**
	 * Affichage des Search Tree's
	 * <p>voir {@link Search_Tree#parcours()}</p>
	 */
	public void affichageST() {
		for(int cpt = 1; cpt < (this.listeST.size()); cpt++){
			System.out.println("-----------------------------------------------");
			this.listeST.get(cpt).parcours();
			System.out.println();
		}
	}
	
	/**
	 * Affichage complet des Search Tree's et Pair Tree's de chaque blocs.
	 * <p>voir {@link #visualisation_liste()}</p>
	 * <p>voir {@link #visualisation_liste_correspondant()}</p>
	 */
	public void visualisation(){
		System.out.println("-------------------------------------Algorithme : Sort By Reversal-------------------------------------");
		this.visualisation_liste();
		this.visualisation_liste_correspondant();
		int a = (this.listePT.size());
		for(int cpt = 1; cpt < a; cpt++){
			System.out.println("-----------------------------------------------");
			System.out.println();
			System.out.println("Bloc n°" + cpt + " :");
			System.out.println();
			System.out.println("Search_Tree associée :");
			System.out.println();
			this.listeST.get(cpt).affichage();
			System.out.println();
			System.out.println("Pair_Tree associée :");
			System.out.println();
			this.listePT.get(cpt).affichage();
			//this.listePT.get(cpt).Parcours();
			System.out.println();
			System.out.println();
		}
	}
	/**
	 * Affichage des entiers représentant les gènes.
	 */
	public void visualisation_liste(){
		System.out.print("Gène(s) :       [ ");
		for(int cpt = 0; cpt < liste.size(); cpt++){
			System.out.print(liste.get(cpt) + " ");
		}
		System.out.println("]");
	}
	/**
	 * Afiichage des correspondants pour chaque gènes.
	 */
	public void visualisation_liste_correspondant(){
		System.out.print("Correspondant : [ ");
		for(int cpt = 0; cpt < listeCorrespondant.size(); cpt++){
			System.out.print(listeCorrespondant.get(cpt) + " ");
		}
		System.out.println("]");
	}
	
	/* -----------------------------------------------------------------
	 * Méthode
	 * -----------------------------------------------------------------
	 */
	
	/**
	 * <p>Fonction encapsulant toutes les méthodes d'initialialisations (l'utilisateur n'instancie que cette classe après avoir ajouté son fichier texte !) </p> 
	 * @throws java.io.IOException
	 * <p>voir {@link #correspondant()}</p>
	 * <p>voir {@link #initialialisation()}</p>
	 * <p>voir {@link #lien}</p>
	 * <p>voir {@link #lienPTC()}</p>
	 * <p>voir {@link #lienPT_ST()}</p>
	 */
	public Lecture() throws java.io.IOException{
		while (lecteur.hasNextInt()){
			liste.add(lecteur.nextInt());
		}
		//Instanciation des correspondants
		this.correspondant();
		//this.affichage_liste();
		this.initialialisation();
		this.lien();
		this.lienPTC();
		this.lienPT_ST();
		//this.inversion();
	}
	/**
	 * Fonction d'initialisation des correspodants. 
	 */
	public void correspondant(){
		int j;
		boolean estTrouvé;
		for(int i=0; i < liste.size()-1; i++){
			j = 0;
			estTrouvé = false;
			while(!(estTrouvé)){ 
				j++;
				if((Math.abs(liste.get(i)) == Math.abs(liste.get(j)) - 1)){
					estTrouvé = true;
				}
			}
			listeCorrespondant.add(j);
		}
	}
	
	/**
	 * Fonction d'initialisation des Search Tree et Pair Tree. 
	 * @throws IOException
	 * <p>voir {@link Search_Tree#Search_Tree()}</p>
	 * <p>voir {@link Search_Tree#setDeb(int)}</p>
	 * <p>voir {@link Search_Tree#setFin(int)}</p>
	 * <p>voir {@link Search_Tree#ajouter(int, int)}</p>
	 * <p>voir {@link Pair_Tree#Pair_Tree()}</p>
	 * <p>voir {@link Pair_Tree#ajouter(int, int)}</p>
	 */
	public void initialialisation() throws IOException{
		double MAX = Math.floor((2*(Math.sqrt((this.getListe().size()/*n*/)*(this.log2(this.getListe().size())/*log n*/)))));
		//int MIN = (int) ((1/2)*Math.sqrt(this.getListe().size()*Math.log(this.getListe().size())));
		//Création du Search_Tree
		Search_Tree inter = null;
		Search_Tree s = null;
		for(int pos = 0; pos < this.getListe().size(); pos++){
			if((pos % MAX) == 0){ // pos % MAX = cpt
				inter = s;
				listeST.add(inter);
				s = new Search_Tree();
				s.setDeb(pos);
				s.ajouter(this.getListe().get(pos), pos);
			}else{
				s.ajouter(this.getListe().get(pos)/*Valeur*/, pos/*élément comparable*/);
				s.setFin(pos);
			}
		}
		listeST.add(s);
		
		//Création Pair_Tree
		//this.affichage_liste();
		Pair_Tree inter2 = null;
		Pair_Tree s2 = null;
		for(int pos2 = 0; pos2 < this.listeCorrespondant.size(); pos2++){
			//System.out.println(pos2);
			if((pos2 % MAX) == 0){
				System.out.println("==================================================================================================================");
				System.out.println("ajout de la valeur : " + listeCorrespondant.get(pos2));
				inter2 = s2;
				listePT.add(inter2);
				s2 = new Pair_Tree();
				s2.ajouter(this.liste.get(pos2), this.listeCorrespondant.get(pos2));
			}else{
				System.out.println("ajout de la valeur : " + listeCorrespondant.get(pos2));
				s2.ajouter(this.liste.get(pos2), this.listeCorrespondant.get(pos2));
			}
		}
		listePT.add(s2);
	}
	
	/**
	 * Fonction établissant les liens dans le Pair Tree tels que x -> x + 1.
	 * <p>voir {@link Pair_Tree#liaison(ArrayList, ArrayList, ArrayList)}</p>
	 * <p>voir {@link #getListe()}</p>
	 * <p>voir {@link #getListeCorrespondant()}</p>
	 * <p>voir {@link #getListePT()}</p>
	 * <p>voir {@link Pair_Tree#parcoursArcOrienté()}</p>
	 */
	public void lienPTC(){
		for(int cpt = 1; cpt < (this.getListePT().size()); cpt++){
			this.listePT.get(cpt).liaison(this.getListe(), this.getListeCorrespondant(), this.getListePT());
		} 
		for(int cpt = 1; cpt < (this.getListePT().size()); cpt++){
			this.listePT.get(cpt).parcoursArcOrienté();
		} 
	}
	/**
	 * Fonction liant les Search Tree's de chaque bloc entre-eux.
	 * <p>voir {@link Search_Tree#setSuccPere(Search_Tree)()}</p>
	 */
	public void lien(){
		int cpt2 = 0;
		for(int cpt = 1; cpt < (this.getListeST().size()-1); cpt++){
				this.listeST.get(cpt).setSuccPere(listeST.get(cpt+1));
				cpt2 = cpt;
		}
		this.listeST.get(cpt2+1).setSuccPere(new Search_Tree());
	}
	
	/**
	 * Fonction fesant le lien entre PT -> ST
	 * <p>voir {@link Pair_Tree#liaison_ST(Search_Tree, ArrayList)}</p>
	 * <p>voir {@link #getListeST()}</p>
	 */
	public void lienPT_ST(){
		for(int cpt = 1; cpt < (this.getListePT().size()); cpt++){
			this.listePT.get(cpt).liaison_ST(this.getListeST().get(cpt), liste);
		}	
	}
	/**
	 * Fonction de calcul du logarithme binaire.
	 * @param x 
	 * @return log binaire de x
	 */
	public int log2(int x){
		return (int) (Math.log(x)/Math.log(2));
	}
	
	
	/**
	 * Fonction effectuant une inversion 
	 * <p>voir {@link Pair_Tree#possèdeArcOrienté()}</p>
	 * <p>voir {@link Pair_Tree#estArcOrienté()}</p>
	 * <p>voir {@link Search_Tree#position()}</p>
	 */
	public void inversion(){
		boolean aTrouvé = false;
		int i = 1;
		Pair_Tree intermediaire = listePT.get(i);		
		Pair_Tree noeudOrienté = null;
		//---------------------------------------------------------------
		//On commence par chercher un arc orienté !
		while(!(aTrouvé)){
			if(intermediaire.possèdeArcOrienté()){
				noeudOrienté = intermediaire.estArcOrienté();
				aTrouvé = true;
			}else{
				i++;
				if(i > listePT.size()){
					return;
				}
				intermediaire = listePT.get(i);
			}
		}
		
		//----------------------------------------------------------------
		//On cherche les positions pour la découpe !
		int posDec1 = noeudOrienté.getST().position();
		int posDec2 = noeudOrienté.getSuccesseur().getST().position();
		//On différencie les 4 cas et on découpe !
		if((noeudOrienté.getVal() > 0 && noeudOrienté.getSuccesseur().getVal() > 0)){
			if(posDec1 < posDec2){
				//On ne prend pas les éléments au deux extrémités
			}else{
				//On prend les deux éléments au extrémité
			}
		}else if((noeudOrienté.getVal() < 0 && noeudOrienté.getSuccesseur().getVal() > 0)){ //Cas imprtant car arc orienté
			if(noeudOrienté.getST().arbre().getVal() != noeudOrienté.getSuccesseur().getST().arbre().getVal()){
				//On ne prend que l'élément de gauche
				//Découpe search tree dans le cas d'une découpe impliquant deux blocs ou plus !
				Search_Tree t1 = new Search_Tree();
				Search_Tree t2 = new Search_Tree();
				Search_Tree t3 = new Search_Tree();
				Search_Tree t4 = new Search_Tree();
				
				
				if (posDec1 < posDec2){
					Search_Tree inter = noeudOrienté.getSuccesseur().getST().arbre();
					//Déclarer arrayliste res
					while(inter.getVal() != noeudOrienté.getST().arbre().getVal()){
						inter = inter.getSuccPere();
						//inverser flags
						//Ajouter inter dans la liste
					}
					//Inverser la liste + changer pointeurs aux extrêmités.
				}else{
					Search_Tree inter = noeudOrienté.getST().arbre();
					//Déclarer arrayliste res
					while(inter.getVal() != noeudOrienté.getSuccesseur().getST().arbre().getVal()){
						inter = inter.getSuccPere();
						//inverser flags
						//Ajouter inter dans la liste
					}
					//Inverser la liste + changer pointeurs aux extrêmités.
				}
				
				noeudOrienté.getST().arbre().découpe(posDec1, t1, t2);
				noeudOrienté.getSuccesseur().getST().arbre().découpe(posDec2, t3, t4);
				t2.setEstInversé(true);
				t3.setEstInversé(true);
				t2.lecture_inverse();
				t3.lecture_inverse();
				Search_Tree fin1 = t1.merge(t3);
				Search_Tree fin2 = t2.merge(t4);
				
				//Découpe Pair Tree
				Pair_Tree pc_1 = new Pair_Tree();
				Pair_Tree pc_2 = new Pair_Tree();
				intermediaire.découpe(posDec1, pc_1, pc_2);
				/*System.out.println("-------------------------------------Arbre T1-------------------------------------------");
				pc_1.affichage();
				/*System.out.println("-------------------------------------Arbre T2-------------------------------------------");
				fin2.affichage();*/
			
			
				/*System.out.println("-------------------------------------Arbre T1-------------------------------------------");
				t1.affichage();
				System.out.println("-------------------------------------Arbre T2-------------------------------------------");
				t2.affichage();
				System.out.println("-------------------------------------Arbre T3-------------------------------------------");
				t3.affichage();
				System.out.println("-------------------------------------Arbre T4-------------------------------------------");
				t4.affichage();*/
			}else{
				//Découpe search tree dans le cas d'une découpe impliquant qu'un seul bloc !
				Search_Tree t1 = new Search_Tree();
				Search_Tree t2 = new Search_Tree();
				Search_Tree t3 = new Search_Tree();
				Search_Tree t4 = new Search_Tree();
				if (posDec1 < posDec2){ // On se retrouve avec t1, t3, t4
					noeudOrienté.getST().arbre().découpe(posDec1, t1, t2);
					t2.découpe(posDec2, t3, t4);
					t3.setEstInversé(true);
					t3.lecture_inverse();
					Search_Tree fin3 = t1.merge(t3).merge(t4);
				}else{// On se retrouve avec t3, t4, t2
					noeudOrienté.getST().arbre().découpe(posDec1, t1, t2);
					t1.découpe(posDec2, t3, t4);
					t4.setEstInversé(true);
					t4.lecture_inverse();
					Search_Tree fin3 = t3.merge(t4).merge(t2);
				}
				
			}
		}else if((noeudOrienté.getVal() > 0 && noeudOrienté.getSuccesseur().getVal() < 0)){ //Cas important car arc orienté
				//On ne prend que l'élément de droite !
		}else{
			if(posDec1 < posDec2){
				//On prend les deux éléments au extrémité
			}else{
				//On ne prend pas les deux éléments au extrémité
			}
		}
		
		//On effectue la fusion des arbres :
	}
}
	


