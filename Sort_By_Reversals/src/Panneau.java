import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Panneau extends JPanel {
    /*
     * Search_Tree
     */
    private Search_Tree arbre;
    
    /**
     * dimensions du Search_Tree 
     */
    private int largeur, hauteur;
     
    /**
     * Coefficients des transformations pour convertir les coordonnées
     * données en nombre de noeuds en coordonnées données en pixels 
     */
    private double dx, dy, mx, my;

    /**
     * On consruit un panneau à partir d'un arbre dont on mémorise
     * une fois pour toutes les dimensions.
     */
    public Panneau(Search_Tree arbre) {
        this.arbre = arbre;
        largeur = arbre.calculerPositions(0, 0);
        hauteur = arbre.hauteur();
        setBackground(Color.WHITE);
    }

    /**
     * Dessin du Search_Tree. 
     * La méthode paint est appelée notamment à l'occasion des changements
     * de taille de la fenêtre. 
     * C'est donc le bon moment pour calculer les coefficients dx, dy, mx, my
     */
    public void paint(Graphics g) {
        super.paint(g);
        if (arbre != null) {
            Dimension d = getSize();
            dx = ((double) d.width) / largeur;
            dy = ((double) d.height) / hauteur;
            mx = dx / 2;
            my = dy / 2;
            this.dessiner(arbre, g);
        }
    }

    /**
     * Dessin 
     */
    private void dessiner(Search_Tree a, Graphics g) {
        final int LARG = 10;
        final int HAUT = 8;

        int x1 = (int) (dx * a.X() + mx);
        int y1 = (int) (dy * a.Y() + my);

        if (a.getFG() != null) {
            Search_Tree f = a.getFG();
            int x2 = (int) (dx * f.X() + mx);
            int y2 = (int) (dy * f.Y() + my);
            g.drawLine(x1, y1, x2, y2);
            dessiner(f, g);
        }
        if (a.getFD() != null) {
            Search_Tree f = a.getFD();
            int x2 = (int) (dx * f.X() + mx);
            int y2 = (int) (dy * f.Y() + my);
            g.drawLine(x1, y1, x2, y2);
            dessiner(f, g);
        }

        Color c = g.getColor();
        g.setColor((a.getFG() == null && a.getFD() == null) ? Color.yellow : Color.pink);
        g.fillRect(x1 - LARG, y1 - HAUT, 2 * LARG, 2 * HAUT);
        g.setColor(c);
        g.drawRect(x1 - LARG, y1 - HAUT, 2 * LARG, 2 * HAUT);

        String s = "" + a.getVal();
        if (s.length() < 2)
            s = " " + s;
        g.drawString(s, x1 - LARG + 3, y1 + HAUT - 2);
    }
}
