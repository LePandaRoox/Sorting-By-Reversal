==Ouverture Rapport==
----------------------
1) Aller dans le dossier Stage.

2) Trouver le fichier rapportStage.pdf.

3) Ouvrir le fichier rapportStage.pdf directement sur gitlab en double cliquant dessus.

==Code Source==
---------------
*) Aller dans le dossier Sort_By_Reversals/src pour accèder aux différentes classes du projet.

1) Télécharger directement le dossier Sort_By_Reversals via gitlab.

2) Importer le projet sous Eclipse.

==Javadoc==
---------------
*) Aller dans le dossier Sort_By_Reversals/doc/index.html pour accèder à la Javadoc.