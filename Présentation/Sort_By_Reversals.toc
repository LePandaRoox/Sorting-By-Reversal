\beamer@endinputifotherversion {3.36pt}
\select@language {french}
\beamer@sectionintoc {1}{Les diff\IeC {\'e}rentes utilisations du tri par inversions}{2}{0}{1}
\beamer@sectionintoc {2}{Pr\IeC {\'e}sentation du probl\IeC {\`e}me : tri par inversions}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Quelques d\IeC {\'e}finitions ...}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Pr\IeC {\'e}sentation}{6}{0}{2}
\beamer@sectionintoc {3}{Historique des progr\IeC {\`e}s du tri par inversions}{7}{0}{3}
\beamer@sectionintoc {4}{Pr\IeC {\'e}sentation de l'algorithme de Hannenhalli and Pevzner}{9}{0}{4}
\beamer@sectionintoc {5}{Pr\IeC {\'e}sentation de la structure pour maintenir une inversion}{12}{0}{5}
\beamer@subsectionintoc {5}{1}{Bloc}{13}{0}{5}
\beamer@subsectionintoc {5}{2}{Pair Tree}{14}{0}{5}
\beamer@subsectionintoc {5}{3}{Search Tree}{15}{0}{5}
\beamer@subsectionintoc {5}{4}{UML de l'impl\IeC {\'e}mentation}{16}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{18}{0}{6}
